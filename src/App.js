import React from "react";
import "./App.css";
import "bulma/css/bulma.css";
import DataFetch from "./DataFetch";

function App() {
  return (
    <div className="App">
      <DataFetch />
    </div>
  );
}

export default App;
