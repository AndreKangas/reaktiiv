import React, { useState, useEffect } from "react";
import axios from "axios";

function DataFetch() {
  const [posts, setPosts] = useState([]);
  const [filteredPosts, setFilteredPosts] = useState([]);
  const [id, setId] = useState(1);
  const [name, setName] = useState("");
  const [orderBy, setOrderBy] = useState(false);

  const handleClick = () => {
    let p = posts.filter((x) => x.id == id);
    setFilteredPosts(p);
  };

  const handleClickSetName = () => {
    let p = posts.filter((x) => x.name.includes(name));
    setFilteredPosts(p);
  };

  const handleClickOrderBy = () => {
    setOrderBy(!orderBy);
    if (orderBy) {
      let sorted = [...filteredPosts].sort((a, b) => {
        if (a.id > b.id) {
          return -1;
        } else if (a.id < b.id) {
          return 1;
        } else {
          return 0;
        }
      });
      console.log(sorted);
      setFilteredPosts(sorted);
    } else {
      let sorted = [...filteredPosts].sort((a, b) => {
        if (a.id > b.id) {
          return 1;
        } else if (a.id < b.id) {
          return -1;
        } else {
          return 0;
        }
      });
      console.log(sorted);
      setFilteredPosts(sorted);
    }
  };

  useEffect(() => {
    axios.get(`https://reaktiiv.ee/beer.json`).then((response) => {
      setPosts(response.data);
      setFilteredPosts(response.data);
    });
  }, []);

  return (
    <div className="container">
      <div className="tile">
        <input type="text" value={id} onChange={(e) => setId(e.target.value)} />
        <button type="button" onClick={handleClick}>
          Fetch By Id
        </button>
      </div>

      <div className="tile is-vertical is-2">
        <input
          className="input"
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <button className="button" type="button" onClick={handleClickSetName}>
          Fetch By Name
        </button>
      </div>
      <br />
      <div className="tile">
        <button
          type="button"
          className="button is-primary is-outlined is-rounded"
          onClick={handleClickOrderBy}
        >
          OrderById
        </button>
      </div>

      <table className="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
        <thead>
          <tr className="is-selected">
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>
          {filteredPosts.map((x) => {
            return (
              <tr key={x.id}>
                <td>{x.id}</td>
                <td>{x.name}</td>
                <td>{x.price}</td>
                <td>{x.description}</td>
                <td>
                  <figure class="image is-128x128">
                    <img src={x.image} />
                  </figure>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default DataFetch;
